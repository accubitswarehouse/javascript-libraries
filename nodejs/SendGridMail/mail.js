/**
 * Created by arun.j on 19/10/16.
 */


//to sendhtml content setcontentType as text/html


function mail() {
    this.fromMail = '';
    this.toMail = '';
    this.apiKey = '';
    this.subject = '';
    this.content = '';
    this.contentType = 'text/plain';
}

mail.prototype.sendMail = function (callBack) {

    var helper = require('sendgrid').mail;
    var from_email = new helper.Email(this.fromMail);
    var to_email = new helper.Email(this.toMail);
    var subject = this.subject;
    var content = new helper.Content(this.contentType, this.content);
    var email = new helper.Mail(from_email, subject, to_email, content);

    var sg = require('sendgrid')(this.apiKey);
    var request = sg.emptyRequest({
        method: 'POST',
        path: '/v3/mail/send',
        body: email.toJSON()
    });

    sg.API(request, function (error, response) {
        // console.log(response.statusCode);
        // console.log(response.body);
        // console.log(response.headers);

        try {
            if (error) {
                var result = {
                    'success': false,
                    'response': response
                };

                if (callBack != undefined) {
                    callBack(result);
                }
            }
            else if (response.statusCode == 202) {
                var result = {
                    'success': true,
                    'message-id': response.headers['x-message-id'],
                    'date': response.headers.date
                }
            }
            else {
                var result = {
                    'success': false,
                    'response': response
                }
            }

            if (callBack != undefined) {
                callBack(result);
            }
        }
        catch (e) {
            console.log(e);
        }

    });

};

module.exports = mail;