/**
 * Created by arun on 22/10/16.
 */


// turn on  ===>    https://www.google.com/settings/security/lesssecureapps  to use with gmail


var nodemailer = require('nodemailer');

function mail() {
    this.fromName = '';
    this.toMail = '';
    this.textContent = '';
    this.html = '';
    this.mailid = '';
    this.password = '';
    this.subject = '';
}

mail.prototype.send = function (callBack) {


    var smtpConfig = {
        pool: true,
        host: 'smtp.gmail.com',
        port: 465,
        secure: true, // use SSL
        auth: {
            user: this.mailid,
            pass: this.password
        }
    };



    var transporter = nodemailer.createTransport(smtpConfig);


    var mailOptions = {
        from: this.fromName,
        to: this.toMail,
        subject: this.subject,
        text: this.textContent,
        html: this.html
    };


    transporter.sendMail(mailOptions, function(error, info){
        if(error){
            console.log("error",error);
             var result = {
                 'success' : false,
                 'error' : error
             };
            callBack(result);

        }
        else {
            console.log('Message sent: ' + info.response);
            var result = {
                'success' : true,
                'status' : info.response
            };
            callBack(result);
        }

    });

};


module.exports = mail;