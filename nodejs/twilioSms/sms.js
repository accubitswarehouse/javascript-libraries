/**
 * Created by arun.j on 22/10/16.
 */

// Twilio Credentials


function sms() {
    this.accountId = '';
    this.token = '';
    this.from = '';
    this.to = '';
    this.mediaUrl = '';
    this.message = '';

}

sms.prototype.send = function (callBack) {

    var client = require('twilio')(this.accountId, this.token);
    client.messages.create({
        to: this.to,
        from: this.from,
        body: this.message,
        // mediaUrl: this.mediaUrl,
    },
     function(err, message) {
        if(err){
            var result = {
                'success' : false,
                'error' : err
            };
            callBack(result);
        }
        else
        {
            var result ={
                'success' : true,
                'id' : message.sid
            };
            callBack(result);
        }
    });
};

module.exports = sms;