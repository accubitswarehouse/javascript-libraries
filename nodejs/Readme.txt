
sendgrid_mail.js
_______________________

Send mails using sendgrid_mail
Requires 'sendgrid' node module
get it by 'npm install --save sendgrid'

usage :

var Mail = require('sendgrid_mail'); // need to provide the full path of the file

var mail = new Mail()
mail.fromMail = aj@accubits.com; 						 // from mail
mail.toMail = letter2aj@gmail.com; 						// to mail
mail.apiKey = 'qwertyuiopasdfghjklzxcvbnm'; 		  // api key from sendrid.com
mail.subject = 'test mail'; 						  // subject
mail.content = 'hi'; 									// content can be either text or htm depend on the content type
mail.contentType = 'text/plain'; 						 //   'text/html' ==> to send html

mail.sendMail(function(result ){

  success result will be 
		{
              'success': true,
            'message-id': message id from sendgrid,
          'date': date from sendgrid response
        }

 errored result will be

			{
                    'success': false,
                   'response': error response
            }



});

########################################################################################################################




Gmail_mailer.js
_______________________

Send mails using gmail
need to adjust the security settings of the account by https://www.google.com/settings/security/lesssecureapps 
Requires 'nodemailer' node module
get it by 'npm install --save nodemailer'

usage : 

var Mail = require('Gmail_mailer'); // need to provide the full path of the file

 var mail = new Mail();
    mail.fromName = 'arun';									// name to be displaye on mail
    mail.toMail = 'aj@accubits.com';						// to mail	
    mail.textContent = 'hi';								// text content
    mail.html = <h>arun</h>;								// if you want to send html use this
    mail.mailid = letter2aj@gmail.com;						// gmail id used to send mail
    mail.password = 'test';									// password of gmail id used to send mail 
    mail.subject = 'hi';									// subject 

    mail.send(function (result) {
       success == > 
	   result will be 
			{
                'success' : true,
                'status' : response id
            }
			
			
		error == >
			result will be
				{
                 'success' : false,
                 'error' : error
				}
			
		
    });

#################################################################################################################################################





sms.js
_______________________



send sms using twilio
requires twilio node module
get it by 'npm install --save twilio'


usage : 


var Sms = require('sms'); // need to provide the full path of the file

var sms = new Sms();
sms.accountId = 'ascdvfggmjhkfgc'; 									// twilio account id
sms.token = 'qwertyuiop'; 										 //twilio token
sms.from = '+621234567890'; 											 // twilio registered/twilio number
sms.to = '+918891414232'; 												  // to number
sms.message = 'hi';										 // message content



 sms.send(function (result) {
      
	  result ==> success
				{
                'success' : true,
                'id' : messageid
				}
	  
	  result ==> error
			{
			  'success' : false,
                'error' : error
			}
	  
	  
    });


