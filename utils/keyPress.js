/**
 * Created by ARUN.J on 27-11-2016.
 */

//  class name of input fields to be binded
// button id of button to be triggered on hitting enter

function bindEnter(input,button){

    var elements = document.getElementsByClassName(input);
    var length = elements.length;
    for(var i=0;i<length;i++){
        elements[i]
            .addEventListener("keyup", function(event) {
                event.preventDefault();
                if (event.keyCode == 13) {
                    document.getElementById(button).click();
                }
            });
    }
}



