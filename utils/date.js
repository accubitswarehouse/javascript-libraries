/**
 * Created by arun on 19/8/16.
 */

 
 function make2digit(digit) {
    if (digit < 10) {
        return '0' + String(digit)
    }
    else {
        return String(digit);
    }
};


// input -- month
// eg : Jan
// output  : 01

function getMonthDigit(month) {

    switch (month) {
        case 'Jan' :
            return '01';
            break;
        case 'Feb' :
            return '02';
            break;
        case 'Mar' :
            return '03';
            break;
        case 'Apr' :
            return '04';
            break;
        case 'May' :
            return '05';
            break;
        case 'Jun' :
            return '06';
            break;
        case 'Jul' :
            return '07';
            break;
        case 'Aug' :
            return '08';
            break;
        case 'Sep' :
            return '09';
            break;
        case 'Oct' :
            return '10';
            break;
        case 'Nov' :
            return '11';
            break;
        case 'Dec' :
            return '12';
            break;
        default :
            return null;
            break;
    }


}


// input -- month number
// eg : 1
// output  : January

function getDigitMonth(month) {

    switch (month) {
        case '1' :
        case '01' :
        case   1 :
            return 'January';
            break;
        case '2' :
        case '02' :
        case   2 :
            return 'February';
            break;
        case '03' :
        case '3' :
        case  3 :
            return 'March';
            break;
        case '04' :
        case '4' :
        case  4 :
            return 'April';
            break;
        case '05' :
        case '5' :
        case  5 :
            return 'May';
            break;
        case '06' :
        case '6' :
        case  6 :
            return 'June';
            break;
        case '07' :
        case '7' :
        case  7 :
            return 'July';
            break;
        case '08' :
        case '8' :
        case  8 :
            return 'August';
            break;
        case '09' :
        case '9' :
        case  9   :
            return 'September';
            break;
        case '10' :
        case  10 :
            return 'October';
            break;
        case '11' :
        case  11 :
            return 'November';
            break;
        case '12' :
        case  12 :
            return 'December';
            break;
        default :
            return null;
            break;
    }


}

// input -- date Object date1 and date2
// output  : differnce between 2 days

var daysBetween = function (date1, date2) {

    var _MS_PER_DAY = 1000 * 60 * 60 * 24;

    var utc1 = Date.UTC(date1.getFullYear(), date1.getMonth(), date1.getDate());

    var utc2 = Date.UTC(date2.getFullYear(), date2.getMonth(), date2.getDate());


    return Math.floor((utc2 - utc1) / _MS_PER_DAY);

};

// input -- month
// eg : 1 or 01 or Jan
// output  : 31

var getMonthDays = function (month) {


    switch (String(month)) {
        case 'Jan' :
        case '1' :
        case '01' :
            return 31;
            break;
        case 'Feb':
        case '2' :
        case '02' :
            return 29;
            break;
        case 'Mar' :
        case '3' :
        case '03' :
            return 31;
            break;
        case 'Apr' :
        case '4' :
        case '04' :
            return 30;
            break;
        case 'May' :
        case '5':
        case '05' :
            return 31;
            break;
        case 'Jun':
        case '6' :
        case '06':
            return 30;
            break;
        case 'Jul' :
        case '7' :
        case '07' :
            return 31;
            break;
        case 'Aug' :
        case '8' :
        case '08' :
            return 31;
            break;
        case 'Sep' :
        case '9'  :
        case '09' :
            return 30;
            break;
        case 'Oct':
        case '10':
            return 31;
            break;
        case 'Nov' :
        case '11' :
            return 30;
            break;
        case 'Dec' :
        case '12' :
            return 31;
            break;
        default :
            return null;
            break;
    }

};


// input -- string (date in format of yy-mm-dd)
// eg : 2016-09-01 2016-09-02
// output  : true


function compareDate(date1, date2) {
    console.log("on date", date1, date2);
    console.log(new Date(date2), new Date(date1));
    return new Date(date2) > new Date(date1);
}




//get date in yy-mm-dd

function getCurrentDate() {
    return String(new Date().getFullYear() + '-' + make2digit(parseInt(new Date().getMonth()) + 1) + '-' + make2digit(parseInt(new Date().getDate())));
}

//get tommorows date in yy-mm-dd

function getNextDate() {
    var outDate = new Date();
    outDate = new Date(outDate.setDate(outDate.getDate() + 1));
    outDate = outDate.getFullYear() + '-' + make2digit(parseInt(outDate.getMonth()) + 1) + '-' + make2digit(parseInt(outDate.getDate()));

    return outDate;

}

//get  date in YYMMDD

function getCurrentDateInYYMMDD(saperator) {
    if (saperator == undefined) {
        saperator = '-';
    }
    return String(new Date().getFullYear() + saperator + make2digit(parseInt(new Date().getMonth()) + 1) + saperator + make2digit(new Date().getDate()));

}

//get  date in DDMMYY

function getCurrentDateInDDMMYY(saperator) {
    if (saperator == undefined) {
        saperator = '-';
    }
    return String(make2digit(new Date().getDate()) + saperator + make2digit(parseInt(new Date().getMonth()) + 1) + saperator + new Date().getFullYear());

}

//get  date in MMDDYY

function getCurrentDateInMMDDYY(saperator) {
    if (saperator == undefined) {
        saperator = '-';
    }
    return String(new Date().getFullYear() + saperator + make2digit(parseInt(new Date().getMonth()) + 1) + saperator + make2digit(new Date().getDate()));

}

//get date in formats
//formats ===> 'm,y,d' 'd,m,y' y,m,d'
//default separator is '-'
function getDate(day, month, year, format, saperator) {
    if (saperator == undefined) {
        saperator = '-';
    }
    if (format == undefined) {
        format = 'y,m,d';
    }
    format = format.replace(/ /g, '');
    var date = new Date(String(year + '-' + make2digit(month) + '-' + make2digit(day)));


    console.log(format);
    console.log(date);
    switch (format) {
        case 'd,m,y' :
            return ( make2digit(parseInt(date.getDate())) + saperator +
            make2digit(parseInt(date.getMonth() + 1)) + saperator +
            date.getFullYear());
            break;
        case 'm,d,y' :
            return ( make2digit(parseInt(date.getMonth() + 1)) + saperator +
            make2digit(parseInt(date.getDate())) + saperator +
            date.getFullYear());
            break;
        case 'y,m,d' :
            return (  date.getFullYear()) + saperator +
                make2digit(parseInt(date.getMonth() + 1)) + saperator +
                make2digit(parseInt(date.getDate()));
            break;
        default :
            return (  make2digit(parseInt(date.getDate())) + saperator +
            make2digit(parseInt(date.getMonth() + 1)) + saperator +
            date.getFullYear());
            break;

    }


}

//get incremented in formats
//formats ===> 'm,y,d' 'd,m,y' y,m,d'
//default separator is '-'

function incrementDateBy(count, day, month, year, format, saperator) {
    if (saperator == undefined) {
        saperator = '-';
    }
    if (format == undefined) {
        format = 'y,m,d';
    }
    format = format.replace(/ /g, '');
    var date = new Date(String(year + '-' + make2digit(month) + '-' + make2digit(day)));
    date = new Date(date.setDate(date.getDate() + count));

    switch (format) {
        case 'd,m,y' :
            return ( make2digit(parseInt(date.getDate())) + saperator +
            make2digit(parseInt(date.getMonth() + 1)) + saperator +
            date.getFullYear());
            break;
        case 'm,d,y' :
            return ( make2digit(parseInt(date.getMonth() + 1)) + saperator +
            make2digit(parseInt(date.getDate())) + saperator +
            date.getFullYear());
            break;
        case 'y,m,d' :
            return ( date.getFullYear() + saperator +
            make2digit(parseInt(date.getMonth() + 1)) + saperator +
            make2digit(parseInt(date.getDate())));
            break;
        default :
            return (  make2digit(parseInt(date.getDate())) + saperator +
            make2digit(parseInt(date.getMonth() + 1)) + saperator +
            date.getFullYear());
            break;

    }


}

//get decremented in formats
//formats ===> 'm,y,d' 'd,m,y' y,m,d'
//default separator is '-'

function decrementDateBy(count, day, month, year, format, saperator) {
    if (saperator == undefined) {
        saperator = '-';
    }
    if (format == undefined) {
        format = 'y,m,d';
    }
    format = format.replace(/ /g, '');
    var date = new Date(String(year + '-' + make2digit(month) + '-' + make2digit(day)));
    date = new Date(date.setDate(date.getDate() - count));

    switch (format) {
        case 'd,m,y' :
            return ( make2digit(parseInt(date.getDate())) + saperator +
            make2digit(parseInt(date.getMonth() + 1)) + saperator +
            date.getFullYear());
            break;
        case 'm,d,y' :
            return ( make2digit(parseInt(date.getMonth() + 1)) + saperator +
            make2digit(parseInt(date.getDate())) + saperator +
            date.getFullYear());
            break;
        case 'y,m,d' :
            return (  date.getFullYear() + saperator +
            make2digit(parseInt(date.getMonth() + 1)) + saperator +
            make2digit(parseInt(date.getDate())));
            break;
        default :
            return (  make2digit(parseInt(date.getDate())) + saperator +
            make2digit(parseInt(date.getMonth() + 1)) + saperator +
            date.getFullYear());
            break;

    }


}

